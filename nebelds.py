from ds         import *

from bidict import bidict
from macro  import *

F_plus       = set()
F_minus      = set()
F_hash       = set()

F_doppelganger_dict = {}

F_O       = set()
UPSILON   = set()

SINGLE_W     = {} # For each action -> corresponding set of UPSILON variables
UPSILON_DICT = {} # For each UPSILON variable -> corresponding effect


EVAL_DICT = {}  # For each action -> corresponding EVAL predicate (stored in F_O)

SYNC = None
TRUE = None
FAIL = None

def add_sync():
    global SYNC
    indx = insertAtom('(running_sync)')
    SYNC = indx

def add_true():
    global TRUE
    indx = insertAtom('(always_true)')
    TRUE = indx


def add_fail():
    global FAIL
    indx = insertAtom('(sync_failure)')    
    FAIL = indx


def sync():
    return SYNC


def fail():
    return FAIL


def true():
    return TRUE


def add_F_O(A):
    for a in A:
        a_indx    = a['name']
        a_name    = get_action_name(a_indx)
        a_pred    = '(eval_{})'.format(a_name)
        indx_pred = insertAtom(a_pred)
        F_O.add(indx_pred)
        EVAL_DICT[a_indx] = indx_pred


def compile_predicates():
    # F_plus (Sigma_+), F_minus (Sigma_-), F_hash (Sigma_#)
    add_copy_of_F()
    add_sync()
    add_true()
    if DATA_DICT['semantics'] == CONTRs:
        add_fail()
    # Upsilon
    add_upsilon_pred()
    # F_O (Sigma_O)
    add_F_O(A)


def add_copy_of_F():
    global F_plus, F_minus, F_hash
    copied_F = set()
    copied_F_dict = {'plus': set(), 'minus': set(), 'hash': set()}
    for f in F:
        F_doppelganger_dict[f] = {}
        for pedix in ['plus', 'minus', 'hash']:
            f_name = get_predicate_str(f)
            # DA CAMBIARE RIMUOVI PARENTESI !!! LE METTI SOLO QUANDO STAMPI
            f_name = f_name.replace('(', '').replace(')', '')
            f_name = '({}_{})'.format(f_name, pedix)
            copied_F.add((f, pedix, f_name))
    
    for f, pedix, f_name in copied_F:
        indx_f = insertAtom(f_name)
        copied_F_dict[pedix].add(indx_f)
        F_doppelganger_dict[f][pedix] = indx_f

    F_plus  = copied_F_dict['plus']
    F_minus = copied_F_dict['minus']
    F_hash  = copied_F_dict['hash']


def add_upsilon_pred():
    upsilon_temp  = set()
    for a in A:
        a_indx = a['name']        
        if (get_action_name(a_indx)) == 'move#airplane_cfbeg#medium#south#seg_rw_0_400#seg_rww_0_50#south':
            print (a)
        SINGLE_W[a_indx] = set()
        counter_eff = 0    
        for w in a[CONDEFFs]:
            when = W[w]
            for k_eff in [ADD, DEL]:
                for eff_indx in when[k_eff]:
                    ups_str = '(upsilon_{}-{})'.format(a_indx, counter_eff)
                    upsilon_temp.add((ups_str, a_indx, eff_indx, k_eff, w))
                    counter_eff += 1
        # Useless
        for k_eff in [ADD, DEL]:
            for eff_indx in a[k_eff]:
                ups_str = '(upsilon_{}-{})'.format(a_indx, counter_eff)
                upsilon_temp.add((ups_str, a_indx, eff_indx, k_eff, None))
                counter_eff += 1

    for ups_str, a_indx, eff_indx, k_eff, w in upsilon_temp:
        ups_indx = insertAtom(ups_str)
        UPSILON.add(ups_indx)
        SINGLE_W[a_indx].add(ups_indx)

        if W[w][PRE] == set():
            prec_cond_eff = set([TRUE])
            pass
        else:
            prec_cond_eff = W[w][PRE]

        UPSILON_DICT[ups_indx] = {
            PRE    : prec_cond_eff,
            'EFF'  : eff_indx,
            'KIND' : k_eff
        }


def compile_initial_state():
    I.add(TRUE)