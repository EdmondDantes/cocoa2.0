from ds         import *


def compile_exp():
    for a in A:
        enumerate_context_and_compile(a, None)


def enumerate_context_and_compile(a, pause):
    a_cond_effs = a[CONDEFFs]

    if len(a_cond_effs) == 1:
        w = list(a_cond_effs)[0]
        if pause is not None:
            a_pre = a[PRE] | set([-1 * pause])
        else:
            a_pre = a[PRE]
        if W[w]['PRE'] == set():
            #insert_comp_action(get_action_name(a['name']), a_pre, a[ADD], a[DEL], A_cost[a['name']])
            insert_comp_action(get_action_name(a['name']), a_pre, W[w][ADD], W[w][DEL], A_cost[a['name']])
            return True

    for int_context, context in enumerate(powerset(a_cond_effs)):
        context     = set(context)
        not_context = a_cond_effs - context
        assert len(a_cond_effs) >= len(context)
        compile_action_EXP(
            a, context, not_context, int_context, A_cost[a['name']], pause)

    return False


def compile_action_EXP(a, context, not_context, int_context, cost, pause):
    comp_a_name     = get_compiled_name_EXP(a['name'], int_context)
    comp_a_pre      = a[PRE] | merge_precondition(context, not_context)
    if pause is not None:
        comp_a_pre  = comp_a_pre | set([-1 * pause])

    comp_a_eff_add  = a[ADD] | merge_effects(context, ADD)
    comp_a_eff_del  = a[DEL] | merge_effects(context, DEL)

    insert_comp_action(comp_a_name, comp_a_pre, comp_a_eff_add, comp_a_eff_del, cost)


def get_compiled_name_EXP(a_indx, int_context):
    a_name      = get_action_name(a_indx)
    a_comp_name = a_name + '__EXP-{}'.format(str(int_context))
    return a_comp_name


def merge_effects(context, kind_e):
    '''
    '''
    eff2add = set()
    for w in context:
        eff2add = eff2add | (W[w][kind_e])
    return eff2add


def merge_precondition(context, no_context):

    pre2add = set()

    for w_indx in context:
        pre2add = pre2add | W[w_indx][PRE]
    for w_indx in no_context:
        pre2add.add(tuple([pre_indx * -1 for pre_indx in W[w_indx][PRE]]))
    return pre2add