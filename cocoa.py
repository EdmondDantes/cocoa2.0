from ds         import *
from math       import copysign
from exp        import *

import networkx          as nx
import matplotlib.pyplot as plt
import heapq


F_O         = set()
EVAL_DICT   = {}

DONE_GEN    = set()

DOPPEL      = set() # Dizionario delle variabili doppiate
DOPPEL_dict = {}
 
PAUSE     = None
DET_CONTR = None

STRUCTURE = None

# AVOID_NASTY_FACTS_FD = False
DETECT_UPFRONT       = False

CHAINs   = []

def compile_cocoa():
    ''' 
    Main Function for compiling using COCOA:
        - simple effects added when the first action is applied
        - setup actions merged togheter with cond. eff. actions
    '''
    global G

    compile_predicates()

    for a in A:
        # detect_mutexes(a)
        compile_action_COCOA(a)

    G.add(pause() * -1)
    if DATA_DICT['semantics'] == 'CONTRs' and DETECT_UPFRONT is False:
        if DATA_DICT['contradictions'] > 0:
            G.add(contr_detection() * -1)

    print_cocoa_statistics()


def detect_mutexes(a):
    ''' To be studied '''
    w_set = (a['COND-EFF'])
    if len(w_set) <= 1:
        return

    w_list = list(w_set)
    for i in range(0, len(w_list) - 1):
        wi = w_list[i]
        pre_i = W[wi][PRE]
        for j in range(i + 1, len(w_set)):
            wj = w_list[j]
            pre_j = W[wj][PRE]

            positive_i = set(l for l in pre_i if l > 0)
            negative_i = set(l for l in pre_i if l < 0)
            positive_j = set(l for l in pre_j if l > 0)
            negative_j = set(l for l in pre_j if l < 0)

            if len((positive_i & negative_j) | (positive_j & negative_i)) > 0:
                print ('MUTEXES')
                exit(1)


def compile_hybrid_cocoa(n_thresh):
    global G
    DATA_DICT['POLY'] = 0
    DATA_DICT['EXP']  = 0
    compile_predicates()

    for a in A:
        compile_action_hybrid_COCOA(a, n_thresh)

    G.add(pause() * -1)

    print_cocoa_statistics()


def compile_predicates():
    '''
    Adding Predicates upfront:
        - pause_condeff
        - evaluating predicates (Sigma_O in Nebel notation)
    '''
    add_pause_predicate()
    add_F_O(A)
    if DETECT_UPFRONT is False:
        if (DATA_DICT['contradictions']) > 0:
            add_contr_detection()


def add_contr_detection():
    global DET_CONTR
    indx      = insertAtom('(detected_contradiction)')    
    DET_CONTR = indx


def contr_detection():
    global DET_CONTR
    return DET_CONTR


def add_F_O(A):
    for a in A:
        a_indx    = a['name']
        a_name    = get_action_name(a_indx)
        a_pred    = '(evaluating_{})'.format(a_name)
        indx_pred = insertAtom(a_pred)
        F_O.add(indx_pred)
        EVAL_DICT[a_indx] = indx_pred


def add_pause_predicate():
    global PAUSE
    indx = insertAtom('(pause_condeff)')
    PAUSE = indx


def pause():
    global PAUSE
    return PAUSE


def compile_action_COCOA(a):
    simple = False
    if OPTIMISATION_DICT['simple-actions']:
        if is_a_simple_action(a):
            generate_plain_action(a)
            simple = True
        else:
            compile_action(a)
    else:
        compile_action(a)
    return simple


def compile_action_hybrid_COCOA(a, n_thresh):
    n_condeff = len(a['COND-EFF'])
    if OPTIMISATION_DICT['simple-actions']:
        n_condeff -= 1 # I do not count the simple CEs

    if n_condeff <= n_thresh:
        simple = enumerate_context_and_compile(a, pause())
        if simple is False:
            DATA_DICT['EXP'] += 1
    else:
        simple = compile_action_COCOA(a)
        if simple is False:
            DATA_DICT['POLY'] += 1



def compile_action(a):
    '''
    Function which compiles a single action with conditional effects
    '''
    if OPTIMISATION_DICT['ss-ces']:
        simplify_static_cond_effs(a)

    first_condeffs, sorting = get_sort(a)
    generate_start_action_a(a)
    start_indx = generate_contradiction_actions(a)
    n          = generate_pos_neg_action_a(a, sorting, first_condeffs, start_indx)
    generate_close_evaluation_a(a, n)
    CHAINs.append(n + 1)


def simplify_static_cond_effs(a):
    add_set  = set()
    del_set  = set()
    w2remove = set()
    for w_indx in a['COND-EFF']:
        w_data = W[w_indx]
        if w_data['PRE'] == set():
            add_set = add_set | w_data[ADD]
            del_set = del_set | w_data[DEL]
            w2remove.add(w_indx)

    a[ADD] = add_set
    a[DEL] = del_set
    for w_indx in w2remove:
        a['COND-EFF'].remove(w_indx)


def is_a_simple_action(a):
    '''
    This function tests if there is just a single
    CE whose precondition is equal to true.
    '''
    if len(a['COND-EFF']) == 1:
        single_w_indx = list(a['COND-EFF'])[0]
        w = W[single_w_indx]
        if (w['PRE'] == set()):
            return True
        else:
            return False
    return False


def compute_precedence_dict(a):
    '''
    Strategemma per evitare che fast-downward generi Nasty Predicates.
    TODO: Trattare gli effetti non condizionali come se lo fossero.
    '''
    precedence_dict = {
        'ADD-act-pre'   : set(),
        'ADD-no-act-pre': set(),
        'DEL-act-pre'   : set(),
        'DEL-no-act-pre': set(),
    }
    for k in [ADD, DEL]:
        for eff in a[k]:
            added = False
            for w_indx in a['COND-EFF']:
                if eff in W[w_indx]['PRE']:
                    precedence_dict['{}-act-pre'.format(k)].add(eff)
                    added = True
                    break
            if not added:
                precedence_dict['{}-no-act-pre'.format(k)].add(eff)
             
    return precedence_dict



def generate_contradiction_actions(a):
    contradictions = a[CONTRs]

    if DETECT_UPFRONT is True:
        return 0
    else:
        if len(contradictions) > 0:
            for indx_contr, contr in enumerate(contradictions):
                add_positive_contradiction(indx_contr, contr, a)
                add_negative_contradiction(indx_contr, contr, a)
        return len(contradictions)


def add_positive_contradiction(indx_contr, contr, a):
    w1 = W[contr[0]]
    w2 = W[contr[1]]

    pre1 = w1[PRE]
    pre2 = w2[PRE]

    a_indx = a['name']
    a_name = get_action_name(a_indx)

    a_name = 'detect_contradictions_pos_{}_{}'.format(a_name, indx_contr)

    evaluating_indx = EVAL_DICT[a_indx]

    prec_a    = set()
    prec_a    = pre1 | pre2 | set([pause(), lazy_done(indx_contr), evaluating_indx])

    eff_a_add = set([DET_CONTR, lazy_done(indx_contr + 1)])
    eff_a_del = set([lazy_done(indx_contr)])

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)


def add_negative_contradiction(indx_contr, contr, a):
    w1 = W[contr[0]]
    w2 = W[contr[1]]

    pre1 = w1[PRE]
    pre2 = w2[PRE]

    a_indx = a['name']
    a_name = get_action_name(a_indx)

    a_name = 'detect_contradictions_neg_{}_{}'.format(a_name, indx_contr)

    evaluating_indx = EVAL_DICT[a_indx]

    prec_a    = set()
    prec_a    = set([pause(), lazy_done(indx_contr), evaluating_indx])
    prec_a.add(tuple([pre_indx * -1 for pre_indx in pre1 | pre2]))

    eff_a_add = set([lazy_done(indx_contr + 1)])
    eff_a_del = set([lazy_done(indx_contr)])

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)


def add_doppel_variables(removed_nodes, a):
    '''
    Function which doubles the variables which need to be doubled
    '''
    if len(removed_nodes) == 0:
        return

    var2replace = set()
    for w in removed_nodes:
        new_vars    = set([abs(x) for x in W[w][PRE]])
        var2replace = var2replace | new_vars
        for v in new_vars:
            if v not in DOPPEL:
                DOPPEL.add(v)
                v_name     = F_bidict[v]
                v_new_name = '(copy_Z_{})'.format(v_name.replace('(', '').replace(')', '')) # CAMBIARE!!!!!!!!!!!!
                indx_copy = insertAtom(v_new_name)
                DOPPEL_dict[v] = indx_copy

    cond_effs = a[CONDEFFs]

    for w in cond_effs:
        w_data  = W[w]
        for x in w_data[PRE]:
            if abs(x) in var2replace:
                W[w][PRE].remove(x)
                W[w][PRE].add(int(copysign(DOPPEL_dict[abs(x)], x)))

    first_condeffs = []

    for var in var2replace:
        first_condeffs.append((var, DOPPEL_dict[var]))
        if not OPTIMISATION_DICT['merge-setup']:
            first_condeffs.append((-1 * var, DOPPEL_dict[var]))

    return first_condeffs


def generate_plain_action(a):
    a_indx = a['name']
    a_name = get_action_name(a_indx)

    prec_a    = a[PRE]
    prec_a    = prec_a | set([-1 * pause()])
    '''
    Extract Single w
    '''
    assert len(a['COND-EFF']) == 1
    single_w_indx = list(a['COND-EFF'])[0]
    w             = W[single_w_indx]

    eff_a_add = w[ADD]
    eff_a_del = w[DEL]
    
    cost      = A_cost[a_indx]

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, cost)  


def generate_close_evaluation_a(a, n):
    a_indx = a['name']
    a_name = get_action_name(a_indx)

    a_name = 'close_eval_{}'.format(a_name)

    evaluating_indx = set([EVAL_DICT[a_indx]])

    prec_a    = set()
    prec_a    = prec_a | set([pause()])
    prec_a    = prec_a | evaluating_indx          # I'm currently evaluating a_indx
    prec_a    = prec_a | set([lazy_done(n)])

    eff_a_add  = set()

    eff_a_del = set([pause(), lazy_done(n)])
    eff_a_del = eff_a_del
    eff_a_del = eff_a_del | evaluating_indx

    eff_a_add = eff_a_add | a[ADD]
    eff_a_del = eff_a_del | a[DEL]

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)  


def generate_pos_neg_action_a(a, sorting, first_condeffs, start_indx):
    ''' 
    This function creates for each conditional effects c = <cond,eff> two actions:
        - an action to apply c when cond is true
        - a set of actions to not apply c when cond is false
    '''
    a_indx      = a['name']
    a_name      = get_action_name(a_indx)

    # Actions for setting the copied variables
    for n, tup_setup in enumerate(first_condeffs, start=start_indx):
        generate_setup_pair(n, tup_setup, a_indx, a_name)

    k = len(first_condeffs) + start_indx

    if len(sorting) == 0:
        # Handle the case in which ss-ces=on and simple-action=off
        return 0
    else:
        for n, i in enumerate(sorting, start = k):
            generate_pos_action_a(i, W[i], n, a_indx, a_name) # positive action
            generate_neg_action_a(i, W[i], n, a_indx, a_name) # negative action
        return n + 1


def generate_setup_pair(n, tup_setup, a_indx, a_name):
    var, copyvar = tup_setup

    if OPTIMISATION_DICT['merge-setup']:
        generate_setup_pair_merged(n, a_indx, a_name, copyvar, var)
    else:
        generate_setup_pair_not_merged(n, a_indx, a_name, copyvar, var)


def generate_setup_pair_not_merged(n, a_indx, a_name, copyvar, var):

    if var < 0: # Negative Predicate
        var_prec   = var
        var        = -var
    elif var > 0:
        var_prec = var

    token      = 'pos' if var_prec > 0 else 'neg'

    var_name   = F_bidict[var].replace('(', '').replace(')', '')
    new_a_name = 'SETUP_POS-{}_{}_{}'.format(
        token, a_indx, var_name)  # RIMUOVI PARENTESI !!!!!!!!!

    prec_a = set([var_prec])
    prec_a = prec_a | set([EVAL_DICT[a_indx], pause(), lazy_done(n), -1 * lazy_done(n + 1)])

    eff_a_add = set([copyvar]) | set([lazy_done(n + 1)])
    eff_a_del = set([lazy_done(n)])

    update_action_COMP(new_a_name, prec_a, eff_a_add, eff_a_del, 0)     

    new_a_name = 'SETUP_NEG-{}_{}_{}'.format(
        token, a_indx, F_bidict[var].replace('(', '').replace(')', ''))  # RIMUOVI PARENTESI !!!!!!!!!

    prec_a = set([-1 * var])
    prec_a = prec_a | set([EVAL_DICT[a_indx], pause(), lazy_done(n), -1 * lazy_done(n + 1)])

    eff_a_add = set([lazy_done(n + 1)])
    eff_a_del = set([lazy_done(n)])

    update_action_COMP(new_a_name, prec_a, eff_a_add, eff_a_del, 0) 


def generate_setup_pair_merged(n, a_indx, a_name, copyvar, var):
    new_a_name = 'SETUP_POS_{}_{}'.format(a_indx, F_bidict[var].replace('(', '').replace(')', ''))  # RIMUOVI PARENTESI !!!!!!!!!

    prec_a = set()
    prec_a = prec_a | set([var])
    prec_a = prec_a | set([EVAL_DICT[a_indx], pause(), lazy_done(n), -1 * lazy_done(n + 1)])

    eff_a_add = set()
    eff_a_add = set([copyvar]) | set([lazy_done(n + 1)])

    eff_a_del = set()
    eff_a_del = eff_a_del | set([lazy_done(n)])

    update_action_COMP(new_a_name, prec_a, eff_a_add, eff_a_del, 0) 

    new_a_name = 'SETUP_NEG_{}_{}'.format(a_indx, F_bidict[var].replace('(', '').replace(')', ''))  # RIMUOVI PARENTESI !!!!!!!!!

    prec_a = set()
    prec_a = prec_a | set([-1 * var])
    prec_a = prec_a | set([EVAL_DICT[a_indx], pause(), lazy_done(n), -1 * lazy_done(n + 1)])

    eff_a_add = set()
    eff_a_add = eff_a_add | set([lazy_done(n + 1)])

    eff_a_del = set()
    eff_a_del = set([copyvar]) | set([lazy_done(n)])

    update_action_COMP(new_a_name, prec_a, eff_a_add, eff_a_del, 0) 



def generate_pos_action_a(i, w, n, a_indx, a_name):
    new_a_name = 'eval_pos_{}_{}'.format(a_name, n)

    prec_a = set()
    prec_a = prec_a | w[PRE]
    prec_a = prec_a | set([EVAL_DICT[a_indx], pause(), lazy_done(n), -1 * lazy_done(n + 1)])

    eff_a_add = set()
    eff_a_add = w[ADD] | set([lazy_done(n + 1)])

    eff_a_del = set()
    eff_a_del = w[DEL] | set([lazy_done(n)])

    update_action_COMP(new_a_name, prec_a, eff_a_add, eff_a_del, 0) 


def generate_neg_action_a(i, w, n, a_indx, a_name):

    if OPTIMISATION_DICT['or-negate']:
        compile_negate_single_action(w, a_indx, n, a_name)

    else:
        compile_negate_multiple_action(w, a_indx, n, a_name)


def compile_negate_single_action(w, a_indx, n, a_name):
    new_a_name = 'eval_neg_{}_{}'.format(a_name, n)

    prec_a = set()
    pre_neg_list = [-1 * x for x in w[PRE]]
    if len(pre_neg_list) > 1:
        pre_neg = tuple(pre_neg_list)
    elif len(pre_neg_list) == 1:
        pre_neg = pre_neg_list[0]
    elif len(pre_neg_list) == 0:
        ''' 
        Simple conditional effects.
        Skip the negative action.
        '''
        return
    prec_a.add(pre_neg)
    prec_a = prec_a | set([EVAL_DICT[a_indx], pause(), lazy_done(n), -1 * lazy_done(n + 1)])
    eff_a_add = set([lazy_done(n + 1)])
    eff_a_del = set([lazy_done(n)])
    update_action_COMP(new_a_name, prec_a, eff_a_add, eff_a_del, 0) 


def compile_negate_multiple_action(w, a_indx, n, a_name):
    new_a_name = 'eval_neg_{}_{}'.format(a_name, n)

    pre_neg_list = [-1 * x for x in w[PRE]]
    if len(pre_neg_list) >= 1:
        pre_neg = pre_neg_list
    elif len(pre_neg_list) == 0:
        ''' 
        Simple conditional effects.
        Skip the negative action.
        '''
        return

    for indx_neg, single_lit_neg in enumerate(pre_neg_list):

        prec_a = set()
        prec_a.add(single_lit_neg)
        prec_a = prec_a | set([EVAL_DICT[a_indx], pause(), lazy_done(n), -1 * lazy_done(n + 1)])

        eff_a_add = set([lazy_done(n + 1)])

        eff_a_del = set([lazy_done(n)])

        update_action_COMP(
            new_a_name + str('_neg-{}'.format(indx_neg)), 
            prec_a, eff_a_add, eff_a_del, 0) 



def generate_start_action_a(a):
    a_indx      = a['name']
    a_name      = get_action_name(a_indx)
    new_a_start = 'start-Sequence___{}'.format(a_name)
    
    # PRE: pre(A) and (not (pause))
    prec_a = a[PRE] | set([-1 * pause()]) 

    if DETECT_UPFRONT:
        prec_a = handle_contradictions_upfront(prec_a, a)

    # ADD-EFF: (pause) and (evaluating A) and (done_0)
    eff_a_add = set([pause(), EVAL_DICT[a_indx], lazy_done(0)])
    # ADD-EFF: empty set
    eff_a_del = set()

    # if AVOID_NASTY_FACTS_FD:
    #     eff_a_add = eff_a_add | precedence_dict['ADD-no-act-pre']
    #     eff_a_del = eff_a_del | precedence_dict['DEL-no-act-pre']

    a_cost = A_cost[a_indx]

    update_action_COMP(new_a_start, prec_a, eff_a_add, eff_a_del, a_cost) 


def handle_contradictions_upfront(prec_a, a):
    contradictions = a[CONTRs]
    if len(contradictions) > 0:
        prec_contradictions = set()
        tup_prec = set()
        for w1, w2 in contradictions:
            tup_prec = W[w1]['PRE'] | W[w2]['PRE']
            tup_prec = tuple(set([-1 * p for p in tup_prec]))
            prec_a.add(tup_prec)
        return prec_a

    elif len(contradictions) == 0:
        return prec_a


def lazy_done(n):
    '''
    I don't generate all DONE predicates upfront, 
    but only when needed.
    '''
    done_str = '(evaluation_done_{})'.format(str(n))
    if n not in DONE_GEN:
        DONE_GEN.add(n)
        indx = insertAtom(done_str)
    else:
        indx = F_bidict.inverse[done_str]
    return indx


def build_inference_graph(wlist):
    '''
    Function which builds the inference graph.
    TO OPTIMISE
    '''
    G = nx.DiGraph()

    for w in wlist:
        G.add_node(w)

    for indx1, w1 in enumerate(wlist):
        for indx2, w2 in enumerate(wlist):
            if indx1 != indx2:

                add_del_w1 = W[w1][ADD] | W[w1][DEL]
                pre_w2     = set([abs(p) for p in W[w2][PRE]])

                if len(add_del_w1 & pre_w2) > 0:
                    G.add_edge(w1, w2)
    return G


def get_sort(a):
    global DATA_DICT
    wlist = a[CONDEFFs]

    G = build_inference_graph(wlist)

    n_edges = G.number_of_edges()
    if n_edges == 0:
        DATA_DICT['w_disj'] += 1
    first_condeffs = []
    if not nx.is_directed_acyclic_graph(G):
        DATA_DICT['w_dcg'] += 1
        G, removed_nodes = remove_cycles_greedy(G)
        first_condeffs   = add_doppel_variables(removed_nodes, a)
    else:
        if n_edges > 0:
            DATA_DICT['w_dag'] += 1
    return first_condeffs, list(nx.topological_sort(G))[::-1] # Inverse ordering


def remove_cycles_greedy(G):
    # nx.draw(G, with_labels=True)
    # plt.show()
    while True:
        sccs_gen = nx.strongly_connected_components(G)
        sccs     = [scc for scc in sccs_gen if len(scc) > 1]
        if len(sccs) == 0:
            break
        scc      = sccs[0]
        R        = set()
        H_list = []
        for n in scc:
            h_n = (G.in_degree(n) + G.out_degree(n)) / (len(W[n][ADD]) + len(W[n][DEL]))
            heapq.heappush(H_list, (n, h_n))
        node2remove = heapq.heappop(H_list)[0]
        edges_to_remove = [(u, v) for u, v in G.in_edges(node2remove)]
        G.remove_edges_from(edges_to_remove)
        R.add(node2remove)
    return G, R


def print_cocoa_statistics():
    print_board ('COCOA STATISTICS', 0, '-')
    print ('\t|A_disj|             : {} ({})'.format(
        DATA_DICT['w_disj'], 
        round(DATA_DICT['w_disj'] / DATA_DICT['gounded_actions_w>0'], 2)))
    print ('\t|A_dag|              : {} ({})'.format(
        DATA_DICT['w_dag'], 
        round(DATA_DICT['w_dag'] / DATA_DICT['gounded_actions_w>0'], 2)))
    print ('\t|A_dcg|              : {} ({})'.format(
        DATA_DICT['w_dcg'], 
        round(DATA_DICT['w_dcg'] / DATA_DICT['gounded_actions_w>0'], 2)))

    print ('\t|Z_variables|        : {}'.format(len(DOPPEL_dict.keys())))
    try:
        print ('\t|Z_variables|/|A_dcg|: {}'.format(round(
            float(len(DOPPEL_dict.keys())) / float(DATA_DICT['w_dcg']), 2)))
    except:
        print ('\t|Z_variables|/|A_dcg|: na')
    if DATA_DICT['w_dcg'] > 0:
        STRUCTURE = 'DCG'
    elif DATA_DICT['w_dag'] > 0:
        STRUCTURE = 'DAG'
    else:
        STRUCTURE = 'DISJ'
    print ('\tStructure            : {}'.format(STRUCTURE))