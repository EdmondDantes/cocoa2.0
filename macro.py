import itertools
import logic
import copy

from logic import get_simplified_formula

import io
from   contextlib import redirect_stdout
from   os.path    import basename, join, getsize


PRE      = 'PRE'
DEL      = 'DEL'
ADD      = 'ADD'

CONDEFFs = 'COND-EFF'

BEAN = '''
#   ██████╗ ██████╗  ██████╗ ██████╗  █████╗ 
#  ██╔════╝██╔═══██╗██╔════╝██╔═══██╗██╔══██╗
#  ██║     ██║   ██║██║     ██║   ██║███████║
#  ██║     ██║   ██║██║     ██║   ██║██╔══██║
#  ╚██████╗╚██████╔╝╚██████╗╚██████╔╝██║  ██║
#   ╚═════╝ ╚═════╝  ╚═════╝ ╚═════╝ ╚═╝  ╚═╝
# 
f.percassi@hud.ac.uk                                                                                                                           
'''

DATA_DICT = {
	'domain_path'                              : None,
	'problem_path'                             : None,
	'domain_name'                              : None,
	'problem_name'                             : None,
	'translation'                              : None,
	'output_path'                              : None,
	'output_domain_path'                       : None,
	'output_problem_path'                      : None,
	'grounded_actions'                         : None,
	'gounded_actions_w>0'                      : 0,
	'grounded_predicates'                      : None,
	'n_condeffs'                               : 0,
	'condeffs_over_actions'                    : None,
	'condeffs_over_actions_with_condeffs'      : None,
	'contradictions'                           : 0,
	'contradictions_over_actions'              : None,
	'contradictions_over_actions_with_condeffs': None,
	'contradictions_plain_eff'                 : 0,
	'actions_with_contradictions'              : 0,
	'grounded_predicates'                      : None,
	'compiled_actions'                         : None,
	'compiled_predicates'                      : None,
	'w_disj'                                   : 0,
	'w_dag'                                    : 0,
	'w_dcg'                                    : 0,
}

OPTIMISATION_DICT = {
	'ss-ces'        : None,
	'or-negate'     : True,
	'merge-setup'   : None,
	'simple-actions': None
}


DELbeforeADD = 'DELbeforeADD'
CONTRs       = 'CONTRs'


def powerset(iterable):
    '''
    Function for generating power-set
    (used by EXP)
    '''
    s = list(iterable)
    return set(itertools.chain.from_iterable(itertools.combinations(s, r) for r in range(len(s)+1)))


def header_test(dom_path, prob_path, out_path, t, semantics):
    ''' 
    Update parameters script 
    '''
    DATA_DICT['domain_path']         = dom_path
    DATA_DICT['problem_path']        = prob_path
    DATA_DICT['domain_name']         = basename(dom_path)
    DATA_DICT['problem_name']        = basename(prob_path)
    DATA_DICT['translation']         = t
    DATA_DICT['semantics']           = semantics
    DATA_DICT['output_domain_path']  = join(out_path, 'compiled_{}'.format(DATA_DICT['domain_name']))
    DATA_DICT['output_problem_path'] = join(out_path, 'compiled_{}'.format(DATA_DICT['problem_name']))

    print (BEAN)
    print_board('PARAMETERS', 0, '-')
    print ('\tDomain path          : {}'.format(dom_path))
    print ('\tProblem path         : {}'.format(prob_path))
    print ('\tTranslation          : {}'.format(t))
    print ('\tSemantics            : {}'.format(semantics))
    print ('\tOutput path          : {}'.format(out_path))
    print ('\t[Output] Domain path : {}'.format(DATA_DICT['output_domain_path']))
    print ('\t[Output] Problem path: {}'.format(DATA_DICT['output_problem_path']))


def print_board(to_print, tab, symb):
	print ('\t' * tab + symb * len(to_print))
	print ('\t' * tab + to_print)
	print ('\t' * tab + symb * len(to_print))


''' TRANSLATIONS NAMES '''
COCOA                    = 'COCOA'
COCOA_off_ss_ces         = 'COCOA(off=ss-ces)'
COCOA_off_merge_setup    = 'COCOA(off=merge-setup)'
COCOA_off_simple_actions = 'COCOA(off=simple-actions)'
BASELINE_COCOA           = 'COCOA(off=ss-ces,off=merge-setup,off=simple-actions)'

HYBRID_COCOA_2           = 'H-COCOA(n=2)'
HYBRID_COCOA_3           = 'H-COCOA(n=3)'
