from macro  import *
from bidict import bidict
from re     import findall


import FDgrounder

'''
    INPUT PROBLEM
'''
F, A, I, G, C = set(), [], set(), set(), set()

F_bidict = bidict()
A_bidict = bidict()
W = []

A_comp        = []
A_comp_bidict = bidict()
A_cost        = {}
A_comp_cost   = {}


BOT_PRED = None

AXIOMS_DICT   = {}

def populate_F(Fgr):
    '''
    Populate F and F_bidict
    '''
    global BOT_PRED
    for atom in Fgr:
        if 'new-axiom@' not in str(atom):
            pddl_atom = atom.to_pddl() # Example: (is-at l1)
            insertAtom(pddl_atom)

    DATA_DICT['grounded_predicates'] = len(F)

    BOT_PRED = insertAtom('(bot_predicate)') # MOVE INTO NEBEL


def populate_axioms_dict(axioms):
    global AXIOMS_DICT
    for ax in axioms:
        axiom_pred = ax.effect.to_pddl().replace(' ', '_')
        if axiom_pred not in AXIOMS_DICT.keys():
            AXIOMS_DICT[axiom_pred] = set()
        conj_ax         = ax.condition
        conj_ax         = set([convert_str_pred_into_indx(str(p)) for p in conj_ax])
        conj_ax_formula = logic.obtain_atom_or_formula(conj_ax, 'AND')
        AXIOMS_DICT[axiom_pred].add(conj_ax_formula)

    for ax_k, ax_value in AXIOMS_DICT.items():
        formula = logic.obtain_atom_or_formula(ax_value, 'OR')
        AXIOMS_DICT[ax_k] = formula

    print ('\t|Axioms|             : {}'.format(len(AXIOMS_DICT.keys())))


def insertAtom(pddl_atom):
    '''
    The indx of pddl_atom is defined
    by the number of stored atoms
    '''
    atom_name = pddl_atom.replace(' ', '_')

    if atom_name not in F_bidict.values():
        n_F = len(F_bidict.keys()) + 1              # Start by 1 for exploiting negative and positive precondition
        F_bidict[n_F] = atom_name                   # Ground atom, i.e., (is-at l1) -> (is-at_l1) 
        F.add(n_F)                                  # Useless? Maybe yes
        return n_F
    else:
        return F_bidict.inverse[atom_name]


def populate_A(Agr, unitcost):
    '''
    Populate A and A_bidict
    '''
    indx_a = 0
    for action in Agr:
        evaluate_action(action)

    DATA_DICT['grounded_actions'] = len(A)


def evaluate_action(action):
    '''
     Example of dumped action:
        (move l1 l0)
        PRE: Atom is-at(l1)
        ADD:  -> Atom is-at(l0)
        ADD: Atom in(o0) -> Atom at(o0, l0)
        ADD: Atom in(o1) -> Atom at(o1, l0)
        DEL:  -> Atom is-at(l1)
        DEL: Atom in(o0) -> Atom at(o0, l1)
        DEL: Atom in(o1) -> Atom at(o1, l1)
        cost: 1
    '''
    with io.StringIO() as buf, redirect_stdout(buf):
        action.dump()
        dumped_action = buf.getvalue()

    aggr_effects = {}
    action_prec  = set()

    dump_pre, dump_eff, cost = get_dump_pre_eff(dumped_action)

    prec_set  = extract_prec_set(dump_pre)
    effs_dict = extract_effs_dict(dump_eff)

    action_name  = action.name.replace(' ', '#').replace('(', '').replace(')', '')

    insertAction(action_name, prec_set, effs_dict, action.cost)


def get_dump_pre_eff(dumped_action):
    '''
    Split the action dump into:
        - PRE statements
        - EFF statements
        - cost
    '''
    dump_pre = []
    dump_eff = []
    cost     = None

    for row in dumped_action.split('\n'):
        if 'DEL: ' in row or 'ADD:' in row:
            dump_eff.append(row)
        elif 'PRE: ' in row:
            dump_pre.append(row)
        elif 'cost: ' in row:
            cost = int(row.replace('cost: ', ''))
    return dump_pre, dump_eff, cost


def extract_prec_set(dump_pre):
    prec_set = set()
    for pre_statement in dump_pre:
        if 'new-axiom@' in pre_statement:
            str_axiom = extract_str_axiom(pre_statement)
            if str_axiom not in AXIOMS_DICT.keys():
                # DA DEFINIRE
                continue
            else:
                print (AXIOMS_DICT[str_axiom])
                exit(1)
        pre_statement = pre_statement.replace('PRE: ', '')
        pre_indx = convert_str_pred_into_indx(pre_statement)
        prec_set.add(pre_indx)
    return prec_set


def extract_str_axiom(pre_statement):
    pre_statement = pre_statement.replace('PRE: ', '')
    assert 'NegatedAtom ' in pre_statement or 'Atom ' in pre_statement
    pre_statement = pre_statement.replace('NegatedAtom ', '').replace('Atom ', '')
    pre_statement = pre_statement.replace('(', ' ').replace(')', '')
    pre_statement = '({})'.format(pre_statement)
    pre_statement = pre_statement.replace(',', '')
    return pre_statement


def extract_effs_dict(dump_effs):
    effs_dict = {
        ADD      : set(),
        DEL      : set(),
        CONDEFFs : set(),
        'CONTRs' : set()
    }

    # Aggreggo gli effetti
    aggr_effects = aggregate_effects(dump_effs)

    for cond_w, eff_dict in aggr_effects.items():

        if is_empty_prec(cond_w): # Simple conditional effect (True > eff)
            for kind_e, eff_list_str in eff_dict.items():
                eff_set = set()
                for eff_str in eff_list_str:
                    eff_set.add(convert_str_pred_into_indx(eff_str))
                effs_dict[kind_e] = eff_set
        else:
            # handle conditional effects
            prec_set_formula_list = generate_cond_w_formula(cond_w)
            for prec_set in prec_set_formula_list:
                add_condeff_with_axiom(effs_dict[CONDEFFs], prec_set, eff_dict)

    '''
    Tested before fixing the simple effect into
    CE with condition equal to True.
    '''
    if len(effs_dict[CONDEFFs]) > 0:
        DATA_DICT['gounded_actions_w>0'] += 1

    fix_simple_ces(effs_dict)

    effs_dict['CONTRs'] = compute_contradictions(
        effs_dict[ADD], effs_dict[DEL], effs_dict[CONDEFFs])

    return effs_dict


def fix_simple_ces(effs_dict):
    '''
    This function adds a special conditional effect
    for handling the simple effects of a. For example:
        INPUT : pre(a), eff(a) = {a,b,<c,d>}
        OUTPUT: pre(a), eff(a) = {<true, {a,b}>, <c,d>}
    '''
    n_W = len(W)
    w_dict = {
        PRE: set(),
        ADD: effs_dict['ADD'],
        DEL: effs_dict['DEL']
    }
    if w_dict[ADD] != set() or w_dict[DEL] != set():
        effs_dict['COND-EFF'].add(n_W)
        effs_dict['ADD'] = set()
        effs_dict['DEL'] = set()
        W.append(w_dict)


def add_condeff_with_axiom(condeffs_dict, prec_set, effs_dict):
    to_extend = None
    for w_indx in condeffs_dict:
        w     = W[w_indx]
        w_pre = w['PRE']
        if prec_set == w_pre:
            to_extend = w_indx
            break

    if to_extend is None:
        condeffs_dict.add(generate_w_indx(prec_set, effs_dict))
        DATA_DICT['n_condeffs'] += 1
    else:
        w['ADD'] = w['ADD'] | set([convert_str_pred_into_indx(e) for e in  effs_dict[ADD]])
        w['DEL'] = w['DEL'] | set([convert_str_pred_into_indx(e) for e in  effs_dict[DEL]])


def compute_contradictions(add_effs, del_effs, condeffs_indx):

    contradictions = set()

    # Effetti condizionali in conflitto
    for w1_indx in condeffs_indx:
        for w2_indx in condeffs_indx:
            if w1_indx == w2_indx:
                continue
            w1 = W[w1_indx]
            w2 = W[w2_indx]

            pre_w1 = w1[PRE]
            pre_w2 = w2[PRE]

            if mutex_formula(pre_w1, pre_w2):
                continue

            conflict_1 = w1[ADD] & w2[DEL]
            conflict_2 = w1[ADD] & w2[DEL]

            if len(conflict_1) > 0 or len(conflict_2) > 0:
            	if not opposition(pre_w1, pre_w2):
                	contradictions.add((w1_indx, w2_indx))
    if len(contradictions) > 0:
        DATA_DICT['actions_with_contradictions'] += 1

    DATA_DICT['contradictions'] += len(contradictions)

    # Effetti non condizionali in conflitto con effetti condizionali
    for w_indx in condeffs_indx:
        w = W[w_indx]
        for add_eff in add_effs:
            if add_eff in w[DEL]:
                contradictions.add((-1, w_indx))
                DATA_DICT['contradictions_plain_eff'] += 1
        for del_eff in del_effs:
            if del_eff in w[ADD]:
                contradictions.add((-1, w_indx))
                DATA_DICT['contradictions_plain_eff'] += 1


    return contradictions


def opposition(pre1, pre2):
	'''
	Given two preconditions (in and),
	detect if they are mutex
	'''
	positive1 = set([x for x in pre1 if x > 0])
	positive2 = set([x for x in pre2 if x > 0])
	negative1 = set([x for x in pre1 if x < 0])
	negative2 = set([x for x in pre2 if x < 0])
	return len(positive1 & negative2) > 0 or len(negative1 & positive2) > 0


def mutex_formula(pre_w1, pre_w2):
    for pre in pre_w1:
        if -1 * pre in pre_w2:
            return True
    else:
        return False


def generate_cond_w_formula(cond_w):
    prec_set = set()
    ATOM_REGEX = r'((?:Atom|NegatedAtom)[^\(]+\([^\)]*\))'
    occs    = list(findall(ATOM_REGEX, cond_w))
    for cond_str in occs:
        if 'new-axiom@' in cond_str:
            axiom_str = extract_str_axiom(cond_str).replace(' ', '_')
            if axiom_str not in AXIOMS_DICT.keys():
                # print ('SEMPRE FALSI')
                # print (cond_w)
                # print (axiom_str)
                continue # CAPIRE COSA SUCCEDE
            axiom_for = copy.deepcopy(AXIOMS_DICT[axiom_str]) # Axiom Formula
            # print ('AXIOM FORMULA')
            # print (axiom_for)
            if 'NegatedAtom' in cond_str:
                axiom_for.negate()
            # print (axiom_for)

            prec_set.add(axiom_for)
        else:
            prec_set.add(convert_str_pred_into_indx(cond_str))

    formula_prec = logic.obtain_atom_or_formula(prec_set, 'AND')

    # print (formula_prec)
    # print (logic.get_dnf4logic(formula_prec))
    return logic.get_dnf4logic(formula_prec) # Return a set of conjuctions


def generate_cond_w(cond_w):
    ''' 
    Caso semplice. Da rimuovere. Versione estesa: generate_cond_w_formula
    '''
    prec_set = set()
    ATOM_REGEX = r'((?:Atom|NegatedAtom)[^\(]+\([^\)]*\))'
    occs = list(findall(ATOM_REGEX, cond_w))
    for cond_str in occs:
        prec_set.add(convert_str_pred_into_indx(cond_str))
    return prec_set


def generate_w_indx(prec_set, eff_dict):
    '''
    '''
    n_W = len(W)
    w_dict = {
        PRE: prec_set,
        ADD: set([convert_str_pred_into_indx(e) for e in  eff_dict[ADD]]),
        DEL: set([convert_str_pred_into_indx(e) for e in  eff_dict[DEL]]),
    }
    W.append(w_dict)
    return n_W



def aggregate_effects(dump_effs):
    '''
    Preprocess for aggregating the
    conditional effect in the action
    dump
    '''
    aggr_effects = {}
    for eff_statement in dump_effs:
        kind_e = 'ADD' if 'ADD: ' in eff_statement else 'DEL' if 'DEL: ' in eff_statement else None
        assert kind_e is not None
        eff_statement = eff_statement.replace('ADD: ', '').replace('DEL: ', '')
        cond_eff      = eff_statement.split(' -> ')
        cond          = cond_eff[0]
        eff           = cond_eff[1]
        if cond not in aggr_effects.keys():
            aggr_effects[cond] = {
                ADD: set(),
                DEL: set()
            }
        aggr_effects[cond][kind_e].add(eff)
    return aggr_effects


def is_empty_prec(cond_w):
    return len(cond_w) == 0


def get_atom_index(ground_atom):
    '''
    '''
    return F_bidict.inverse[ground_atom]


def insertAction(action_name, prec_set, effs_dict, cost):
    '''
    '''
    n_A = len(A_bidict.keys())

    if action_name in A_bidict.values():
        return

    A_bidict[n_A] = action_name

    action_dict = {
        'name'    : n_A,
        'PRE'     : prec_set,
        'ADD'     : effs_dict[ADD],
        'DEL'     : effs_dict[DEL],
        CONDEFFs  : effs_dict[CONDEFFs],
        CONTRs    : effs_dict[CONTRs]
    }

    A_cost[n_A] = cost

    A.append(action_dict)


def insert_comp_action(comp_a_name, comp_a_pre, comp_a_eff_add, comp_a_eff_del, cost):
    '''
    '''
    n_A_comp = len(A_comp_bidict.keys())
    A_comp_bidict[n_A_comp] = comp_a_name

    comp_action_dict = {
        'name'    : n_A_comp,
        'PRE'     : comp_a_pre,
        'ADD'     : comp_a_eff_add,
        'DEL'     : comp_a_eff_del,
    }
    A_comp_cost[n_A_comp] = cost
    A_comp.append(comp_action_dict)


def extract_indxs_conditions(str_cond):
    '''
    '''
    ATOM_REGEX = r'^(Atom|NegatedAtom)\S+\(([^\(\)]*)\)$'
    indexes_cond = []
    for str_atom in (findall(ATOM_REGEX, str_cond)):
        index = convert_str_pred_into_indx(str_atom)
        indexes_cond.append(index)
    return indexes_cond


def extract_indxs_eff(str_effs):
    '''
    Cambia nome str_eff -> list o set a seconda dei casi
    '''
    indexes_effs = set()
    for str_atom in str_effs:
        indexes_effs.add(convert_str_pred_into_indx(str_atom))
    return indexes_effs


def convert_str_pred_into_indx(str_atom):
    '''

    '''
    kind_p    = 'NegatedAtom ' if 'NegatedAtom ' in str_atom else 'Atom ' if 'Atom ' in str_atom else None
    assert kind_p is not None
    str_atom  = str_atom.replace(kind_p, '')
    kind_p_m  = -1 if kind_p == 'NegatedAtom ' else 1 if kind_p == 'Atom ' else None
    assert kind_p_m is not None
    str_atom  = str_atom.replace(kind_p, '')
    b_indx    = str_atom.find('(')
    atom_name = str_atom[0:b_indx].replace(' ', '') # CHECK SETTLER PERCHE USO REPLACE?
    pars      = str_atom[b_indx:].replace('(', '').replace(')', '').replace(' ', '').split(',')
    g_atom    = '({})'.format(atom_name + '_' + '_'.join(pars))

    try:
        index = insertAtom(g_atom)
    except:
        index =  get_atom_index(g_atom)

    return index * kind_p_m


def get_action_name(a_indx):
    return A_bidict[a_indx]


def populate_I(Igr):
    for i in Igr:
        I.add(get_atom_index(i.to_pddl().replace(' ', '_')))
        # I.add(get_atom_index(to_pddl(i).replace(' ', '_')))


def populate_G(Ggr):
    ''' 
    Generalize 
    To extend / control
    '''
    with io.StringIO() as buf, redirect_stdout(buf):
        Ggr.dump()
        dump_G = buf.getvalue()

    G_temp = set()

    for row in dump_G.split('\n'):
        if row == '' or 'Atom' not in row:
            continue
        row = row.replace('    ', '')
        G_temp.add(convert_str_pred_into_indx(row))

    manipulate_goal(G_temp, dump_G)

def manipulate_goal(G_temp, dump_G):
    global G
    dump_G_rows = dump_G.split('\n')
    first_row   = dump_G_rows[0]
    kind = 1
    if 'Disjunction' in first_row:
        kind = -1

    for g in G_temp:
        G.add(kind * g)

    '''
    Hacky solution. At the moment FDGrounder returns
    a disjuction goal of one atom and there is no possibility
    of understading if it is a disjunction or conjuction. There
    we check: if g in I, then we negate g.
    '''
    if len(G) == 1:
        g = list(G)[0]
        if g in I:
            G.remove(g)
            G.add(- 1 * g)


def get_predicate_str(indx_p):
    return F_bidict[indx_p]


def update_action_COMP(aname, pre, eff_add, eff_del, cost):
    n_A_T                = len(A_comp)
    A_comp_bidict[n_A_T] = aname
    new_action = {
        'name': n_A_T,
        PRE   : pre,
        ADD   : eff_add,
        DEL   : eff_del
    }
    A_comp.append(new_action)
    A_comp_cost[n_A_T] = cost