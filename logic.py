from sympy import parse_expr, symbols, to_dnf


class Atom:
    
    def __init__(self, variable):
        assert type(variable) == int
        self.variable = variable


    def __str__(self):
        return str(self.variable)
    

    def negate(self):
        self.variable = -self.variable


    def get_recursive_symbols(self, el, symbols_s):
        symbols_s.add(self.variable)


    def human_readable_str(self):
        if self.variable < 0:
            symb = '~'
        elif self.variable > 0:
            symb = ''
        else:
            print ('Error - Atom.human_readable_str')
            exit(1)
        return symb + 'L' + str(abs(self.variable))


class Formula:
    
    def __init__(self, elements, connective):
        integer    = set([e for e in elements if type(e) == int])
        no_integer = set([e for e in elements if e not in integer])
        self.elements   = set([Atom(e) for e in integer]) | no_integer
        self.connective = connective


    def __str__(self):
        str2return = ''
        str2return += '(' + self.connective + ' '
        for el in self.elements:
            str2return += str(el) + ' '
        str2return += ')'
        return str2return


    def negate(self):
        if self.connective == 'AND':
            self.connective = 'OR'
        elif self.connective =='OR':
            self.connective = 'AND'
        for el in self.elements:
            el.negate()


    def simplify_single_formula(self):
        if len(self.elements) == 1:
            self = list(self.elements)[0]
        for el in self.elements:
            el.simplify_single_formula()


    def get_symbols(self):
        symbols_s = set()

        self.get_recursive_symbols(self.elements, symbols_s)
        
        return symbols_s


    def get_recursive_symbols(self, input_, symbols_s):
        for el in self.elements:
            el.get_recursive_symbols(el, symbols_s)


    def human_readable_str(self):
        if self.connective == 'AND':
            symb = '&'
        elif self.connective == 'OR':
            symb = '|'
        return '({})'.format(symb.join([hr.human_readable_str() for hr in self.elements]))


def obtain_atom_or_formula(set_of_something, connective):
    if len(set_of_something) == 1:
        element = list(set_of_something)[0]
        if type(element) == int:
            return Atom(list(set_of_something)[0])
        elif type(element) == Atom:
            return element
        elif type(element) == Formula:
            return element
    else:
        return Formula(set_of_something, connective)


def get_simplified_formula(formula):
    formula.simplify_single_formula()
    if len(formula.elements) == 1:
        return list(formula.elements)[0]
    else:
        return formula


def get_dnf4logic(formula):
    formula_str = formula.human_readable_str()

    formula     = parse_expr(formula_str)
    dnf_formula = str(to_dnf(formula_str))
    ''' 
    Low level solution (portable) 
    '''
    disjs       = dnf_formula.split('|')
    prec_list   = []
    for disj in disjs:
        conjs = set([clean_l(l) for l in disj.split('&')])
        prec_list.append(conjs)
        
    return prec_list


def clean_l(l):
    kind = 1
    if '~' in l:
        kind = -1
    l = l.replace('(', '').replace(')', '')
    l = l.replace(' ', '').replace('L', '')
    l = l.replace('~', '')
    return kind * int(l)

