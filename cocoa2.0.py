


from ds         import *
from FDgrounder import ground
from macro      import *
from nebel      import *
from nebelstar  import *
from cocoa      import *
from pprint     import pprint

import click

import FDgrounder

DEFAULT_TRANSLATION = 'COCOA[off=ss-ces]'
DEFAULT_TRANSLATION = 'COCOA[off=simple-actions]'
DEFAULT_TRANSLATION = 'COCOA[off=simple-actions,off=ss-ces]'

DEFAULT_TRANSLATION = 'EXP'
DEFAULT_TRANSLATION = 'COCOA'
DEFAULT_TRANSLATION = 'H-COCOA[n=3]'
DEFAULT_TRANSLATION = 'NEBELSTAR'


DEFAULT_SEMANTICS   = CONTRs

TRANSLATION         = None

''' 
PARAMETERS
''' 


DOMAIN_PATH  = 'INPUT/domain.pddl'
PROBLEM_PATH = 'INPUT/sample.pddl'


@click.command()
@click.option('--domain_path',  required=True, type=click.Path(exists=True), help='PDDL Domain  fullpath.',              default=DOMAIN_PATH)
@click.option('--problem_path', required=True, type=click.Path(exists=True), help='PDDL Problem fullpath.',              default=PROBLEM_PATH)
@click.option('--output_path',  required=True, type=click.Path(exists=True), help='Output fullpath.',                    default='OUTPUT/')
@click.option('--translation',  required=True, type=str,                     help='Translation in {NEBEL, COCOA, EXP}',  default=DEFAULT_TRANSLATION)
@click.option('--semantics',    required=True, type=str,                     help='Semantics in {DELbeforeADD, CONTRs}', default=DEFAULT_SEMANTICS)


def main(domain_path, problem_path, output_path, translation, semantics):
    global TRANSLATION, SEMANTICS
    TRANSLATION = translation
    header_test(
        domain_path, 
        problem_path, 
        output_path, 
        translation, 
        semantics)

    unit_cost = parse(domain_path, problem_path)
    set_optimisations(translation)
    compile(translation)
    save(unit_cost)


def set_optimisations(translation):
    OPTIMISATION_DICT['ss-ces']         = not 'off=ss-ces' in translation
    OPTIMISATION_DICT['merge-setup']    = not 'off=merge-setup' in translation
    OPTIMISATION_DICT['simple-actions'] = not 'off=simple-actions' in translation
    #OPTIMISATION_DICT['merge-setup']    = False


    print_board ('ACTIVE OPTIMISATIONS', 0, '-')
    print ('\tApply static CEs at the end of the sequence: {}'.format(
        OPTIMISATION_DICT['ss-ces']))
    print ('\tUntouch actions with static CEs            : {}'.format(
        OPTIMISATION_DICT['simple-actions']))
    print ('\tMerge Setup Actions                        : {}'.format(
        OPTIMISATION_DICT['merge-setup']))
    if OPTIMISATION_DICT['or-negate']:
        print ('\tSingle Negated Actions (or)')
    else:
        print ('\tMultiple Negated Actions')
    print ()


def parse(domain_path, problem_path):
    '''
    Parse with FDGrounder and then populate
    the data structure F, A, I, G
    '''
    with io.StringIO() as buf, redirect_stdout(buf):
        Fgr, Agr, Igr, Ggr, Cgr, axioms = FDgrounder.ground(domain_path, problem_path)

    unit_cost = check_unit_cost(Agr)

    # for a in axioms:
    #     print (a)

    populate_axioms_dict(axioms)
    print ('\n* Generating Predicates')
    populate_F(Fgr)                 # Predicates
    print ('* Generating Actions\n')
    populate_A(Agr, unit_cost)      # Actions
    populate_I(Igr)                 # Initial State
    populate_G(Ggr)                 # Goal State

    '''
    Print statistics about grounding 
    '''
    print_board('GROUNDING STATISTICS', 0, '-')
    print ('\t|A|                   : {}'.format(DATA_DICT['grounded_actions']))
    print ('\t|A_W|                 : {}'.format(DATA_DICT['gounded_actions_w>0']))
    print ('\t|A_simple|            : {}'.format(DATA_DICT['grounded_actions'] - DATA_DICT['gounded_actions_w>0']))
    print ('\t|F|                   : {}'.format(DATA_DICT['grounded_predicates']))
    print ('\t|W|                   : {}'.format(DATA_DICT['n_condeffs']))
    # Number of conditional effects per action
    print ('\t|W|/|A|               : {}'.format(
        round(DATA_DICT['n_condeffs'] / DATA_DICT['grounded_actions'], 2)))
    # Number of conditional effects per action having conditional effects
    n_avg = round(DATA_DICT['n_condeffs'] / DATA_DICT['gounded_actions_w>0'], 2)
    print ('\t|W|/|A_W|             : {}'.format(n_avg))
    print ('\tSize                  : {}'.format(get_size_w(n_avg)))
    print ('\t|contradictions|      : {}'.format(DATA_DICT['contradictions']))
    print ('\t|A_contradictions|    : {}'.format(DATA_DICT['actions_with_contradictions']))
    # Number of contradictions per action
    # print ('\t|contradictions|/|A|: {}'.format(
    #     round(DATA_DICT['contradictions'] / DATA_DICT['grounded_actions'])))
    # Number of contradictions per action having conditional effects 
    print ('\t|contradictions|/|A_W|: {}'.format(
        round(DATA_DICT['contradictions'] / DATA_DICT['gounded_actions_w>0'], 2)))
    print ('\t|contradictions_plain|: {}'.format(DATA_DICT['contradictions_plain_eff']))
    print ('\tUnit-Costs            : {}'.format(unit_cost))
    print ()

    return unit_cost


def get_size_w(n_avg):
    if n_avg <= 2:
        return 'TINY'
    elif n_avg > 2 and n_avg <= 4:
        return 'SMALL'
    elif n_avg > 4 and n_avg <= 6:
        return 'MEDIUM'
    elif n_avg > 6 and n_avg <= 8:
        return 'BIG'
    elif n_avg > 8:
        return 'HUGE'


def check_unit_cost(Agr):
    costs_set = set([a.cost for a in Agr])

    if len(costs_set) > 1:
        return False
    else:
        return True


def compile(translation):
    if translation == 'EXP':
        compile_exp()
    elif translation == 'NEBEL':
        compile_nebel()
    elif 'H-COCOA' in translation:
        compile_hybrid_cocoa(get_threshold(translation))
    elif 'COCOA' in translation:
        compile_cocoa()
    elif translation == 'NEBELSTAR':
        compile_nebelstar()

    DATA_DICT['compiled_actions']    = len(A_comp)
    DATA_DICT['compiled_predicates'] = len(F)

    print_board ('COMPILATION STATISTICS', 0, '-')
    print ('\t|A_T|      : {}'.format(DATA_DICT['compiled_actions']))
    print ('\t|F_T|      : {}'.format(DATA_DICT['compiled_predicates']))
    print ('\t|A_T|/|A|  : {}'.format(
        round(DATA_DICT['compiled_actions'] / DATA_DICT['grounded_actions'], 2)))
    print ('\t|F_T|/|F|  : {}'.format(
        round(DATA_DICT['compiled_predicates'] / DATA_DICT['grounded_predicates'], 2)))

    if 'H-COCOA' in translation:
        print ('\tHybrid COCOA:')
        print ('\t\t* Compiled(COCOA): {} ({}%)'.format(
            DATA_DICT['POLY'], 
            100 * round(DATA_DICT['POLY'] / DATA_DICT['gounded_actions_w>0'], 2)))
        print ('\t\t* Compiled(EXP)  : {} ({}%)'.format(
            DATA_DICT['EXP'],
            100 * round(DATA_DICT['EXP'] / DATA_DICT['gounded_actions_w>0'], 2)))

    print ()


def get_threshold(t):
    return int(t.split('[')[1].split('=')[1].replace(']', ''))


def generate_action(a, unit_cost):
    a_str  = '\t(:action ' + A_comp_bidict[a['name']] + '\n'
    a_str += '\t\t:parameters()\n'
    a_str += '\t\t:precondition (and'

    for pre in a['PRE']:
        if type(pre) == int: # Singola variabile in AND
            a_str += str(axiom_to_string(pre))
        elif type(pre) == tuple:
            if len(pre) > 1:
                a_str += '(or '
            for pred in pre:
                a_str += str(axiom_to_string(pred))
            if len(pre) > 1:
                a_str += ')'

    a_str += '\t)\n'

    a_str += '\t\t:effect (and'

    for eff_indx in a['ADD']:
        a_str += ' {}'.format(F_bidict[eff_indx])
    for eff_indx in a['DEL']:
        a_str += ' (not {})'.format(F_bidict[eff_indx])

    if not (unit_cost is True and TRANSLATION == 'EXP'):
        a_cost = int(A_comp_cost[a['name']])
        if a_cost > 0:
            a_str += ' (increase (total-cost) {})'.format(str(a_cost))

    a_str += ')\n'
    a_str += '\t)\n\n'
    return a_str


def axiom_to_string(pre):
    global AXIOM_DICT
    pre_str = F_bidict[abs(pre)]

    if pre < 0:
        extemplate = ' (not {})'
    else:
        extemplate = ' {}'

    if 'new-axiom@' in pre_str:
        exit(1)
    else:
        return extemplate.format(pre_str)




def save(unit_cost):
    print_board('Saving compiled domain/problem', 0, '-')

    estimated_size, dom_str, output_domain_path = save_domain(unit_cost)
    save_problem(unit_cost)

    print ('\tEstimated Size [MB]: {}'.format(estimated_size))

    if estimated_size <= 2000:
        with open(output_domain_path, 'w') as fout:
            fout.write(dom_str)
        domain_size_MB  = round(getsize(DATA_DICT['output_domain_path']) / (1024 * 1024), 3)
        print ('\tDomain Size    [MB]: {}'.format(domain_size_MB))
    else:
        print ('\tDomain Size    [MB]: > 2000')



def save_domain(unit_cost):
    dom_str  = '(define '

    dom_str += '(domain compiled_domain)\n\n'
    dom_str += '\t(:requirements :strips)\n\n'
    dom_str += '\t(:predicates\n'

    for f in F:
        f_str = get_predicate_str(f)
        if 'new-axiom@' not in f_str:
            dom_str += '\t\t' + f_str + '\n'
    dom_str += '\t)\n\n'
    
    if not (unit_cost is True and TRANSLATION == 'EXP'):
        dom_str += '\t(:functions (total-cost) - number)\n\n'

    for a in A_comp:
        dom_str += generate_action(a, unit_cost)
    
    dom_str += ')'

    dom_str = dom_str.replace('@', '_AT_')

    output_domain_path = DATA_DICT['output_domain_path']

    return round(len(dom_str.encode('utf-8')) / (1024 * 1024), 3), dom_str, output_domain_path


def save_problem(unit_cost):

    prob_str  = '(define '

    prob_str += '(problem compiled_problem)\n\n'
    prob_str += '\t(:domain compiled_domain)'

    prob_str += '\t(:init\n'
    for i in I:
        prob_str += '\t\t' + get_predicate_str(i) + '\n'
    prob_str += '\t)\n\n'

    prob_str += '\t(:goal (and\n'
    for g in G:
        prob_str += '\t\t'
        if g < 0:
            template = '(not {})'
        else:
            template = '{}'
        prob_str += template.format(get_predicate_str(abs(g))) + '\n'
    prob_str += '\t))\n'

    if not (unit_cost is True and TRANSLATION == 'EXP'):
        prob_str += '\t(:metric minimize (total-cost))\n\n'

    prob_str += '\n)'

    prob_str = prob_str.replace('@', '_AT_')

    output_domain_path = DATA_DICT['output_problem_path']

    with open(output_domain_path, 'w') as fout:
        fout.write(prob_str)


if __name__ == "__main__":
    main()