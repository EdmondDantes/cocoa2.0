from bidict     import bidict
from nebelds    import *

import nebelds

NEBEL_PAUSE = None

DONE_GEN      = set()

NEBEL_GROUPBY = {}
UPS_BY_ACTION = {}


def add_nebel_pause():
    global NEBEL_PAUSE
    indx = insertAtom('(neb_pause)')
    NEBEL_PAUSE = indx

def compile_nebelstar():
    compile_predicates_star()
    add_nebel_pause()
    compile_actions_NEBEL_star()
    compile_initial_state()
    compile_goal_state()


def compile_actions_NEBEL_star():
    # preprocess_actions()    
    for a in A:
        generate_start_action_a(a)
        ce_indx = generate_pos_neg_action_a(a)
        ce_indx = 0
        generate_close_evaluation_a(a, ce_indx + 1)
    for f, f_dict in F_doppelganger_dict.items():
        generate_synchronisation_a(f, f_dict)
    generate_close_synch()


def generate_close_evaluation_a(a, ce_indx):
    a_indx = a['name']
    a_name = get_action_name(a_indx)

    a_name = 'close_eval_{}'.format(a_name)

    evaluating_indx = set([EVAL_DICT[a_indx]])

    prec_a    = set()
    prec_a    = prec_a | evaluating_indx          # I'm currently evaluating a_indx
    prec_a    = prec_a | UPS_BY_ACTION[a_indx]    # All the conditional effect have to be evaluated
    # prec_a    = prec_a | set([lazy_done(ce_indx)])
    prec_a    = prec_a | set([NEBEL_PAUSE])

    eff_a_add = set()
    eff_a_add = eff_a_add | set([nebelds.SYNC])

    eff_a_del = set()
    eff_a_del = eff_a_del | evaluating_indx
    # eff_a_del = eff_a_del | set([lazy_done(ce_indx)])

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)


def compile_predicates_star():
    # F_plus (Sigma_+), F_minus (Sigma_-), F_hash (Sigma_#)
    add_copy_of_F()
    add_sync()
    add_true()
    if DATA_DICT['semantics'] == CONTRs:
        add_fail()
    # Upsilon
    add_upsilon_pred_star()
    # F_O (Sigma_O)
    add_F_O(A)


def add_upsilon_pred_star():
    upsilon_temp  = set()
    for a in A:
        a_indx = a['name']

        if (get_action_name(a_indx)) == 'move#airplane_cfbeg#medium#south#seg_rw_0_400#seg_rww_0_50#south':
            print (a)

        UPS_BY_ACTION[a_indx] = set()
        counter_eff = 0    
        for w in a[CONDEFFs]:
            when = W[w]
            for k_eff in [ADD, DEL]:
                for eff_indx in when[k_eff]:
                    ups_str = '(upsilon_{}-{})'.format(a_indx, counter_eff)
                    upsilon_temp.add((ups_str, a_indx, eff_indx, k_eff, w))
                    counter_eff += 1
        # Useless
        for k_eff in [ADD, DEL]:
            for eff_indx in a[k_eff]:
                ups_str = '(upsilon_{}-{})'.format(a_indx, counter_eff)
                upsilon_temp.add((ups_str, a_indx, eff_indx, k_eff, None))
                counter_eff += 1

    for ups_str, a_indx, eff_indx, k_eff, w in upsilon_temp:
        if a_indx not in NEBEL_GROUPBY.keys():
            NEBEL_GROUPBY[a_indx] = {}
        if w is not None:
            windx = w
        elif w is None:
            windx = 'STATIC'
        if windx not in NEBEL_GROUPBY[a_indx].keys():
            NEBEL_GROUPBY[a_indx][windx] = set()

        ups_indx = insertAtom(ups_str)
        UPSILON.add(ups_indx)
        NEBEL_GROUPBY[a_indx][windx].add((eff_indx, k_eff, ups_indx))
        UPS_BY_ACTION[a_indx].add(ups_indx)


def generate_start_action_a(a):
    a_indx      = a['name']
    a_name      = get_action_name(a_indx)
    new_a_start = 'start-Sequence___{}'.format(a_name)

    prec_a = set()
    prec_a = prec_a | a[PRE] | set([-1 * NEBEL_PAUSE])

    prec_a = prec_a | set([-1 * sync()])

    eff_a_add = set()
    eff_a_add = eff_a_add | set([EVAL_DICT[a_indx]]) | set([lazy_done(0)])
    eff_a_add = eff_a_add | set([NEBEL_PAUSE])

    eff_a_del = set()

    cost      = A_cost[a_indx]

    update_action_COMP(new_a_start, prec_a, eff_a_add, eff_a_del, cost)


def generate_pos_neg_action_a(a):
    a_indx = a['name']
    a_name = get_action_name(a_indx)

    for w_indx, effs_dict in NEBEL_GROUPBY[a_indx].items():
        generate_pos_action_a_star(w_indx, a_indx, a_name, effs_dict)
        generate_neg_action_a_star(w_indx, a_indx, a_name, effs_dict)


def generate_neg_action_a_star(w_indx, a_indx, a_name, effs_set):

    a_name = 'eval_neg_{}_{}'.format(a_name, w_indx)

    prec_a    = set()
    prec_a    = prec_a | set([EVAL_DICT[a_indx]])        # I'm currently evaluating a_indx
    prec_a.add(tuple([x * -1 for x in W[w_indx][PRE]]))
    prec_a    = prec_a | set([NEBEL_PAUSE])

    eff_a_add = set()
    for aff_ups, k_eff, ups in effs_set:
        eff_a_add = eff_a_add | set([ups])

    # eff_a_add = eff_a_add | set([ups_indx])              # Keep track that I am evaluated ups_indx conditional effect
    # eff_a_add = eff_a_add | set([lazy_done(ce_indx + 1)])
    eff_a_del = set()
    # eff_a_del = eff_a_del | set([lazy_done(ce_indx)])
    
    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)   


def generate_pos_action_a_star(w_indx, a_indx, a_name, effs_set):
    a_name = 'eval_pos_{}_{}'.format(a_name, w_indx)
    
    prec_a    = set()
    prec_a    = prec_a | set([EVAL_DICT[a_indx]]) # I'm currently evaluating a_indx
    prec_a    = prec_a | W[w_indx][PRE]           # Condition of the ups_indx conditional effect
    prec_a    = prec_a | set([NEBEL_PAUSE])

    eff_a_add     = set()

    for aff_ups, k_eff, ups in effs_set:
        eff_a_add = eff_a_add | set([ups])
        doppelgangers = F_doppelganger_dict[aff_ups]
        if k_eff == ADD:
            k_f = 'plus'
        elif k_eff == DEL:
            k_f = 'minus'
        eff_a_add = eff_a_add | set([doppelgangers[k_f], doppelgangers['hash']])

    eff_a_del = set()
    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)   


# def generate_pos_action_a(ups_indx, a_indx, a_name, ce_indx):

#     ups          = UPSILON_DICT[ups_indx]
#     k_eff        = ups['KIND']
#     ups_affected = ups['EFF']

#     a_name = 'eval_pos_{}_{}_{}'.format(
#         a_name, ups_indx, get_predicate_str(ups_affected).replace(')', '').replace('(', '')) # CAMBIARE!!!!!!!!!!!    

#     prec_a    = set()
#     prec_a    = prec_a | set([EVAL_DICT[a_indx]]) # I'm currently evaluating a_indx
#     prec_a    = prec_a | ups[PRE]                 # Condition of the ups_indx conditional effect
#     prec_a    = prec_a | set([NEBEL_PAUSE])
#     prec_a    = prec_a | set([lazy_done(ce_indx)])

#     eff_a_add = set()
#     eff_a_add = eff_a_add | set([ups_indx])       # Keep track that I am evaluated ups_indx conditional effect
#     eff_a_add = eff_a_add | set([lazy_done(ce_indx + 1)])

#     if k_eff == ADD:
#         k_f = 'plus'
#     elif k_eff == DEL:
#         k_f = 'minus'

#     doppelgangers = F_doppelganger_dict[ups_affected]

#     eff_a_add = eff_a_add | set([doppelgangers[k_f], doppelgangers['hash']])
#     eff_a_del = set()
#     eff_a_del = eff_a_del | set([lazy_done(ce_indx)])

#     update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)   


def generate_neg_action_a(ups_indx, a_indx, a_name, ce_indx):

    ups = UPSILON_DICT[ups_indx]

    a_name = 'eval_neg_{}_{}'.format(a_name, ups_indx)    

    prec_a    = set()
    prec_a    = prec_a | set([EVAL_DICT[a_indx]])        # I'm currently evaluating a_indx
    prec_a.add(tuple([x * -1 for x in ups[PRE]]))
    prec_a    = prec_a | set([NEBEL_PAUSE])
    prec_a    = prec_a | set([lazy_done(ce_indx)])
    # Condition of the ups_indx conditional effect

    eff_a_add = set()
    eff_a_add = eff_a_add | set([ups_indx])              # Keep track that I am evaluated ups_indx conditional effect
    eff_a_add = eff_a_add | set([lazy_done(ce_indx + 1)])
    eff_a_del = set()
    eff_a_del = eff_a_del | set([lazy_done(ce_indx)])

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)


def generate_synchronisation_a(f_indx, doppelgangers):    
    generate_pos_synch_f(f_indx, doppelgangers)
    generate_neg_synch_f(f_indx, doppelgangers)
    generate_fail_synch_f(f_indx, doppelgangers)


def generate_fail_synch_f(f_indx, doppelgangers):
    global SEMANTICS
    f_name        = get_predicate_str(f_indx)
    a_name        = 'synch_fail_{}'.format(f_name.replace('(', '').replace(')', '')) # CAMBIARE !!!!!!!!

    prec_a = set()
    prec_a = prec_a | set([nebelds.SYNC])
    prec_a = prec_a | set([doppelgangers['plus'], doppelgangers['minus'], doppelgangers['hash']])
    eff_a_add = set()

    if DATA_DICT['semantics'] == CONTRs:
        eff_a_add = eff_a_add | set([fail()])
    elif DATA_DICT['semantics'] == DELbeforeADD:
        eff_a_add = eff_a_add | set([f_indx])

    eff_a_del = set()

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)  


def generate_pos_synch_f(f_indx, doppelgangers):
    f_name        = get_predicate_str(f_indx)
    a_name        = 'synch_positive_{}'.format(f_name.replace('(', '').replace(')', '')) # CAMBIARE !!!!!!!!

    prec_a = set()
    prec_a = prec_a | set([nebelds.SYNC])
    prec_a = prec_a | set([doppelgangers['plus'], -1 * doppelgangers['minus'], doppelgangers['hash']])
    prec_a = prec_a | set([NEBEL_PAUSE])

    eff_a_add = set()
    eff_a_add = eff_a_add | set([f_indx])

    eff_a_del = set()
    eff_a_del = eff_a_del | set([doppelgangers['hash']])

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)  


def generate_neg_synch_f(f_indx, doppelgangers):
    f_name        = get_predicate_str(f_indx)
    a_name        = 'synch_negative_{}'.format(f_name.replace('(', '').replace(')', '')) # CAMBIARE !!!!!!!!

    prec_a = set()
    prec_a = prec_a | set([nebelds.SYNC])
    prec_a = prec_a | set([-1 * doppelgangers['plus'], doppelgangers['minus'], doppelgangers['hash']])
    prec_a = prec_a | set([NEBEL_PAUSE])

    eff_a_add = set()

    eff_a_del = set()
    eff_a_del = eff_a_del | set([f_indx, doppelgangers['hash']])

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)  


def generate_close_synch():
    global F_hash
    a_name = 'close_seq_condeff'

    prec_a = set()
    prec_a = prec_a | set([nebelds.SYNC])
    prec_a = prec_a | set([x * -1 for x in nebelds.F_hash])
    prec_a = prec_a | set([NEBEL_PAUSE])

    eff_a_add = set()
    eff_a_del = set()
    eff_a_del = eff_a_del | set([nebelds.SYNC]) | nebelds.F_plus |  nebelds.F_minus | UPSILON | set([NEBEL_PAUSE])

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)


def compile_goal_state():
    for x in [x * -1 for x in F_O]:
        G.add(x)
    G.add(-1 * nebelds.SYNC)
    if DATA_DICT['semantics'] == CONTRs:
        G.add(-1 * nebelds.FAIL)
    G.add(-1 * NEBEL_PAUSE)


def lazy_done(n):
    '''
    I don't generate all DONE predicates upfront, 
    but only when needed.
    '''
    done_str = '(evaluation_done_{})'.format(str(n))
    if n not in DONE_GEN:
        DONE_GEN.add(n)
        indx = insertAtom(done_str)
    else:
        indx = F_bidict.inverse[done_str]
    return indx