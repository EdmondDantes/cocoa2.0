
from bidict     import bidict
from macro      import *

import nebelds

from nebelds import *

def compile_nebel():
    compile_predicates()
    compile_actions_NEBEL()
    compile_initial_state()
    compile_goal_state()


def compile_goal_state():
    for x in [x * -1 for x in F_O]:
        G.add(x)
    G.add(-1 * nebelds.SYNC)
    if DATA_DICT['semantics'] == CONTRs:
        G.add(-1 * nebelds.FAIL)


def generate_close_evaluation_a(a):
    a_indx = a['name']
    a_name = get_action_name(a_indx)

    a_name = 'close_eval_{}'.format(a_name)

    evaluating_indx = set([EVAL_DICT[a_indx]])

    prec_a    = set()
    prec_a    = prec_a | evaluating_indx          # I'm currently evaluating a_indx
    prec_a    = prec_a | SINGLE_W[a_indx]         # All the conditional effect have to be evaluated

    eff_a_add = set()
    eff_a_add = eff_a_add | set([nebelds.SYNC])

    eff_a_del = set()
    eff_a_del = eff_a_del | evaluating_indx

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)


def compile_actions_NEBEL():
    preprocess_actions()    
    for a in A:
        generate_start_action_a(a)
        generate_pos_neg_action_a(a)
        generate_close_evaluation_a(a)
    for f, f_dict in F_doppelganger_dict.items():
        generate_synchronisation_a(f, f_dict)
    generate_close_synch()


def preprocess_actions():
    '''
    Function that removes the plain effects representing
    them as conditional effects
    '''
    for a in A:

        add_effs = a['ADD']
        del_effs = a['DEL']

        n_W = len(W)

        w_dict = {
            PRE: set(),
            ADD: add_effs,
            DEL: del_effs,
        }
        W.append(w_dict)
        a['ADD'] = set()
        a['DEL'] = set()
        a[CONDEFFs].add(n_W)


def generate_w_indx(prec_set, eff_dict):
    '''
    '''
    n_W = len(W)
    w_dict = {
        PRE: prec_set,
        ADD: set([convert_str_pred_into_indx(e) for e in  eff_dict[ADD]]),
        DEL: set([convert_str_pred_into_indx(e) for e in  eff_dict[DEL]]),
    }
    W.append(w_dict)
    return n_W


def generate_pos_neg_action_a(a):
    a_indx = a['name']
    a_name = get_action_name(a_indx)
    for ups_indx in SINGLE_W[a_indx]:
        generate_pos_action_a(ups_indx, a_indx, a_name)
        generate_neg_action_a(ups_indx, a_indx, a_name)


def generate_synchronisation_a(f_indx, doppelgangers):    
    generate_pos_synch_f(f_indx, doppelgangers)
    generate_neg_synch_f(f_indx, doppelgangers)
    generate_fail_synch_f(f_indx, doppelgangers)


def generate_fail_synch_f(f_indx, doppelgangers):
    global SEMANTICS
    f_name        = get_predicate_str(f_indx)
    a_name        = 'synch_fail_{}'.format(f_name.replace('(', '').replace(')', '')) # CAMBIARE !!!!!!!!

    prec_a = set()
    prec_a = prec_a | set([nebelds.SYNC])
    prec_a = prec_a | set([doppelgangers['plus'], doppelgangers['minus'], doppelgangers['hash']])
    eff_a_add = set()

    if DATA_DICT['semantics'] == CONTRs:
        eff_a_add = eff_a_add | set([fail()])
    elif DATA_DICT['semantics'] == DELbeforeADD:
        eff_a_add = eff_a_add | set([f_indx])

    eff_a_del = set()

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)  


def generate_pos_synch_f(f_indx, doppelgangers):
    f_name        = get_predicate_str(f_indx)
    a_name        = 'synch_positive_{}'.format(f_name.replace('(', '').replace(')', '')) # CAMBIARE !!!!!!!!

    prec_a = set()
    prec_a = prec_a | set([nebelds.SYNC])
    prec_a = prec_a | set([doppelgangers['plus'], -1 * doppelgangers['minus'], doppelgangers['hash']])

    eff_a_add = set()
    eff_a_add = eff_a_add | set([f_indx])

    eff_a_del = set()
    eff_a_del = eff_a_del | set([doppelgangers['hash']])

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)  


def generate_neg_synch_f(f_indx, doppelgangers):
    f_name        = get_predicate_str(f_indx)
    a_name        = 'synch_negative_{}'.format(f_name.replace('(', '').replace(')', '')) # CAMBIARE !!!!!!!!

    prec_a = set()
    prec_a = prec_a | set([nebelds.SYNC])
    prec_a = prec_a | set([-1 * doppelgangers['plus'], doppelgangers['minus'], doppelgangers['hash']])

    eff_a_add = set()

    eff_a_del = set()
    eff_a_del = eff_a_del | set([f_indx, doppelgangers['hash']])

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)  


def generate_close_synch():
    a_name = 'close_seq_condeff'

    prec_a = set()
    prec_a = prec_a | set([nebelds.SYNC])
    prec_a = prec_a | set([x * -1 for x in nebelds.F_hash])

    eff_a_add = set()
    eff_a_del = set()
    eff_a_del = eff_a_del | set([nebelds.SYNC])

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)  


def generate_start_action_a(a):
    a_indx      = a['name']
    a_name      = get_action_name(a_indx)
    new_a_start = 'start-Sequence___{}'.format(a_name)

    prec_a = set()
    prec_a = prec_a | a[PRE]

    prec_a = prec_a | set([-1 * eval_pred for eval_pred in F_O]) | set([-1 * sync()])

    eff_a_add = set()
    eff_a_add = eff_a_add | set([EVAL_DICT[a_indx]])

    eff_a_del = set()

    eff_a_del = eff_a_del | nebelds.F_plus | nebelds.F_minus | nebelds.F_hash | UPSILON

    cost      = A_cost[a_indx]

    update_action_COMP(new_a_start, prec_a, eff_a_add, eff_a_del, cost)   


def generate_pos_action_a(ups_indx, a_indx, a_name):

    ups          = UPSILON_DICT[ups_indx]
    k_eff        = ups['KIND']
    ups_affected = ups['EFF']

    a_name = 'eval_pos_{}_{}_{}'.format(
        a_name, ups_indx, get_predicate_str(ups_affected).replace(')', '').replace('(', '')) # CAMBIARE!!!!!!!!!!!    

    prec_a    = set()
    prec_a    = prec_a | set([EVAL_DICT[a_indx]]) # I'm currently evaluating a_indx
    prec_a    = prec_a | ups[PRE]                 # Condition of the ups_indx conditional effect

    eff_a_add = set()
    eff_a_add = eff_a_add | set([ups_indx])       # Keep track that I am evaluated ups_indx conditional effect

    if k_eff == ADD:
        k_f = 'plus'
    elif k_eff == DEL:
        k_f = 'minus'

    doppelgangers = F_doppelganger_dict[ups_affected]

    eff_a_add = eff_a_add | set([doppelgangers[k_f], doppelgangers['hash']])
    eff_a_del = set()

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)   


def generate_neg_action_a(ups_indx, a_indx, a_name):

    ups = UPSILON_DICT[ups_indx]

    a_name = 'eval_neg_{}_{}'.format(a_name, ups_indx)    

    prec_a    = set()
    prec_a    = prec_a | set([EVAL_DICT[a_indx]])        # I'm currently evaluating a_indx

    prec_a.add(tuple([x * -1 for x in ups[PRE]]))

    # Condition of the ups_indx conditional effect

    eff_a_add = set()
    eff_a_add = eff_a_add | set([ups_indx])              # Keep track that I am evaluated ups_indx conditional effect
    eff_a_del = set()

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)   


def generate_close_evaluation_a(a):
    a_indx = a['name']
    a_name = get_action_name(a_indx)

    a_name = 'close_eval_{}'.format(a_name)

    evaluating_indx = set([EVAL_DICT[a_indx]])

    prec_a    = set()
    prec_a    = prec_a | evaluating_indx          # I'm currently evaluating a_indx
    prec_a    = prec_a | SINGLE_W[a_indx]         # All the conditional effect have to be evaluated

    eff_a_add = set()
    eff_a_add = eff_a_add | set([nebelds.SYNC])

    eff_a_del = set()
    eff_a_del = eff_a_del | evaluating_indx

    update_action_COMP(a_name, prec_a, eff_a_add, eff_a_del, 0)   


