# COCOA Readme

Cocoa is a compiler for compiling away conditional effects in a classical planning problem.

# How to run COCOA
To call the compiler use the following command:

    python cocoa2.0.py
        --domain_path  DOMAIN_PATH
        --problem_path PROBLEM_PATH
        --output_path  OUTPUT_PATH
        --translation  TRANSLATION
        --semantics    SEMANTICS

where DOMAIN_PATH, PROBLEM_PATH are self explanatory. 

TRANSLATION is a parameter chosen among:
 - EXP for using the compilation provided by Gazen and Knoblock (1997);
 - NEBEL for using the compilation provided by Nebel (2000);
 - COCOA

SEMANTICS is a parameter for controlling the semantics for the effects:
 - DELbeforeADD for using the common semantics *delete-before-add*, i.e., if there are two conflicting effects then the positive one prevails;
 - CONTRs for using a contradiction-based semantics, i.e., if there are two conflicting effects then it is generated a contradiction making the problem unsolvable.
 
## Note on Semantics
At the current stage, the compiler has been tested exclusively with problems featuring non-conflicting conditional effects. As a result, the use of semantics such as DelBeforeAdd or CONTRs has no effect for the COCOA translation. 
However, NEBEL's translation is originally provided to support contradiction-based semantics. Nevertheless, the implementation allows its usage with *delete-before-add* semantics as well.

## How to cite us [Cocoa]
    @inproceedings{cocoa,
      title        = {An Effective Polynomial Technique for Compiling Conditional Effects Away},
      author       = {Gerevini, Alfonso Emilio and Percassi, Francesco and Scala, Enrico},
      booktitle    = {Proceedings of the Thirty-Eighth {AAAI} Conference on Artificial Intelligence},
      year         = {2024},
      pages        = {20104--20112},
      publisher    = {{AAAI} Press},
    }
    
## References
> **Authors:** Gazen, B. C., & Knoblock, C. A. > **Title:** *Combining the expressivity of UCPOP with the efficiency of Graphplan* > **Conference:** *European Conference on Planning* > **Year of Publication:** 1997 > **Pages:** 221-233 > 
>
> **Author:** Nebel, B. > **Title:** *On the compilability and expressive power of propositional planning formalisms* > **Journal:** *Journal of Artificial Intelligence Research* > **Volume:** 12 > **Year of Publication:** 2000 > **Pages:** 271-315 

## Credits

This project incorporates parts of the **[Fast Downward Grounder](https://www.fast-downward.org/)**, which is distributed under the **GNU General Public License (GPL) version 3 or later**.  

